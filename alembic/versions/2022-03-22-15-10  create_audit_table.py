"""create audit table

Revision ID: 446c001afbf6
Revises: 2a9f396426ce
Create Date: 2022-03-22 15:10:25.434787

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
from sqlalchemy import func

revision = '446c001afbf6'
down_revision = '2a9f396426ce'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('auditoria_produtos',
                    sa.Column('codigo', sa.Integer, primary_key=True, index=True),
                    sa.Column('produto_id', sa.Integer, index=True),
                    sa.Column('estoque_id', sa.Integer, index=True),
                    sa.Column('preco_por', sa.Float),
                    sa.Column('preco_de', sa.Float),
                    sa.Column('produto_nome', sa.String),
                    sa.Column('estoque_total', sa.Integer),
                    sa.Column('estoque_disponivel', sa.Integer),
                    sa.Column('estoque_de_corte', sa.Integer),
                    sa.Column('created_at', sa.DateTime, server_default=func.now()),
                    sa.Column('operation', sa.String(20))
                    )


def downgrade():
    op.drop_table('auditoria_produtos')
