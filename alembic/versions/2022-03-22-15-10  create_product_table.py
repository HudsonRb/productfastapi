"""create product table

Revision ID: e0bbe9b07ed9
Revises: 
Create Date: 2022-03-22 15:10:13.122065

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
from sqlalchemy import func

revision = 'e0bbe9b07ed9'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('produtos', sa.Column('codigo', sa.Integer, primary_key=True, autoincrement=True, index=True),
                    sa.Column('nome', sa.String(50), unique=True, index=True, nullable=False),
                    sa.Column('created_at', sa.DateTime, default=func.now(), onupdate=None),
                    sa.Column('updated_at', sa.DateTime, default=func.now(), onupdate=func.now()),
                    sa.Column('preco_de', sa.Float, nullable=False),
                    sa.Column('preco_por', sa.Float, nullable=False))


def downgrade():
    op.drop_table('produtos')
