"""create estoque table

Revision ID: 2a9f396426ce
Revises: e0bbe9b07ed9
Create Date: 2022-03-22 15:10:17.280946

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2a9f396426ce'
down_revision = 'e0bbe9b07ed9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('estoque', sa.Column('codigo', sa.Integer, primary_key=True, autoincrement=True, index=True),
                    sa.Column('produto_id', sa.Integer, sa.ForeignKey('produtos.codigo')),
                    sa.Column('estoque_total', sa.Integer, nullable=False),
                    sa.Column('estoque_de_corte', sa.Integer, nullable=False),
                    sa.Column('estoque_disponivel', sa.Integer, nullable=False)
                    )


def downgrade():
    op.drop_table('estoque')
