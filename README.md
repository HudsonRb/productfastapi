# ProductFastApi



## Dependencias
 - docker
 - docker-compose

## Running intructions

```
git clone https://gitlab.com/HudsonRb/productfastapi.git &&
cd productfastapi &&
sudo docker-compose up -d --build
```

** Os serviços serão dispostos nas portas db:5444 e web:8008
** Como foi criado em fast api, tem um swagger disposto para teste em http://localhost:8008/docs

## Disclaimer

- Por não entender os termos e contextos em que se aplicam, talvez as regras de negócios não estejam condisentes com o que foi solicitado.
- Quaisquer dúvidas, estou a disposição.
- Estava mais familiarizado com flask, e nunca havia feito a modelagem de um banco de dados via alembic, foi um desafio.
- Levei um tempo também para entender o "schemas" do pydantic, no flask usava o marshmallow para tal.
- Essa foi a primeira audit table que criei, não sei se o certo foi fazer o event listener do sqlalchemy, ou se o certo era criar "triggers" no banco.
- Não fiz o projeto no padrão TDD por pegar o projeto para desenvolver de fato na sexta(não é desculpa sou total responsável por isso).