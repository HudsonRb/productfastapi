from datetime import datetime

from asyncpg import Connection
from sqlalchemy import Column, Integer, String, Float, DateTime, event
from sqlalchemy.orm import relationship, Mapper

from api.db_config import Base
from api.models.audit_table import AuditTable
from api.models.estoque import Estoque


class Produtos(Base):
    __tablename__ = 'produtos'

    codigo = Column(Integer, primary_key=True, index=True)
    nome = Column(String, unique=True, index=True)
    created_at = Column(DateTime, default=datetime.now, onupdate=None)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    preco_de = Column(Float, nullable=False)
    preco_por = Column(Float, nullable=False)
    estoque = relationship(Estoque, back_populates='produtos', uselist=False)

    __mapper_args__ = {"eager_defaults": True}


@event.listens_for(Produtos, 'after_insert')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Produtos):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        produto_id=target.codigo,
        preco_de=target.preco_de,
        preco_por=target.preco_por,
        produto_nome=target.nome,
        operation='insert'))


@event.listens_for(Produtos, 'after_update')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Produtos):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        produto_id=target.codigo,
        preco_de=target.preco_de,
        preco_por=target.preco_por,
        produto_nome=target.nome,
        operation='update'))


@event.listens_for(Produtos, 'before_delete')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Produtos):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        produto_id=target.codigo,
        preco_de=target.preco_de,
        preco_por=target.preco_por,
        produto_nome=target.nome,
        operation='delete'))
