from sqlalchemy import Column, Integer, ForeignKey, event
from sqlalchemy.future import Connection
from sqlalchemy.orm import relationship, Mapper

from api.db_config import Base
from api.models.audit_table import AuditTable


class Estoque(Base):
    __tablename__ = 'estoque'
    codigo = Column(Integer, primary_key=True, index=True, autoincrement=True)
    estoque_total = Column(Integer, nullable=False)
    estoque_de_corte = Column(Integer, nullable=False)
    estoque_disponivel = Column(Integer, nullable=False)
    produto_id = Column(Integer, ForeignKey('produtos.codigo'))
    produtos = relationship('Produtos', back_populates='estoque')


@event.listens_for(Estoque, 'after_insert')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Estoque):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        estoque_id=target.codigo,
        produto_id=target.produto_id,
        estoque_total=target.estoque_total,
        estoque_de_corte=target.estoque_de_corte,
        estoque_disponivel=target.estoque_disponivel,
        operation='insert'))


@event.listens_for(Estoque, 'after_update')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Estoque):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        estoque_id=target.codigo,
        produto_id=target.produto_id,
        estoque_total=target.estoque_total,
        estoque_de_corte=target.estoque_de_corte,
        estoque_disponivel=target.estoque_disponivel,
        operation='update'))


@event.listens_for(Estoque, 'before_delete')
def insert_into_audit_table(mapper: Mapper, connection: Connection, target: Estoque):
    po = AuditTable.__table__
    connection.execute(po.insert().values(
        estoque_id=target.codigo,
        produto_id=target.produto_id,
        estoque_total=target.estoque_total,
        estoque_de_corte=target.estoque_de_corte,
        estoque_disponivel=target.estoque_disponivel,
        operation='delete'))
