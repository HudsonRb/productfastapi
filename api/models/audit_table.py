from sqlalchemy import Column, Integer, ForeignKey, Float, String, DateTime, func

from api.db_config import Base


class AuditTable(Base):
    __tablename__ = 'auditoria_produtos'

    codigo = Column(Integer, primary_key=True, index=True)
    preco_de = Column(Float)
    preco_por = Column(Float)
    produto_nome = Column(String(200))
    produto_id = Column(Integer)
    estoque_id = Column(Integer)
    estoque_total = Column(Integer)
    estoque_de_corte = Column(Integer)
    estoque_disponivel = Column(Integer)
    created_at = Column(DateTime, default=func.now())
    operation = Column(String(20))
