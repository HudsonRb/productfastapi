from pydantic import BaseModel

from api.schemas.estoque_schema import EstoqueSchemaOut, EstoqueSchemaIn


class ProductBaseSchema(BaseModel):
    codigo: int
    nome: str
    preco_de: float
    preco_por: float


class ProductSchemaIn(ProductBaseSchema):
    estoque: EstoqueSchemaIn


class ProductSchemaOut(ProductBaseSchema):
    estoque: EstoqueSchemaOut

    class Config:
        orm_mode = True


class ProductUpdate(ProductBaseSchema):
    codigo: int
    estoque: EstoqueSchemaIn
    nome: str | None
    preco_de: float | None
    preco_por: float | None
