from pydantic import BaseModel


class EstoqueBase(BaseModel):
    estoque_total: int
    estoque_de_corte: int


class EstoqueSchemaOut(EstoqueBase):
    estoque_disponivel: int

    class Config:
        orm_mode = True


class EstoqueSchemaIn(EstoqueBase):
    estoque_total: int
    estoque_de_corte: int
