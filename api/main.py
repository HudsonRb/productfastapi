from typing import List

import uvicorn
from fastapi import FastAPI, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from api.db_config import get_session
from api.models.produtos import Produtos
from api.schemas.product_schema import ProductSchemaOut, ProductSchemaIn, ProductUpdate
from api.services import product_services

app = FastAPI()


@app.get('/')
async def root():
    return {'message': 'Welcome to FAST_API'}


@app.get('/produtos/id/{codigo}', response_model=ProductSchemaOut)
async def product(codigo: int, db: AsyncSession = Depends(get_session)):
    produto: Produtos = await product_services.get_product(db, codigo)
    return produto


@app.get('/produtos', response_model=List[ProductSchemaOut | None])
async def products(db: AsyncSession = Depends(get_session)):
    lista = await product_services.get_all_products(db)
    return lista


@app.post('/produtos', response_model=ProductSchemaOut)
async def product(produto: ProductSchemaIn, db: AsyncSession = Depends(get_session)):
    produto_criado = await product_services.create_product(db=db, produto=produto)
    return produto_criado


@app.delete('/produtos', status_code=202)
async def delete_product(prod_id: int, db: AsyncSession = Depends(get_session)):
    await product_services.delete_product(db, prod_id)
    return {'Message': 'Deleted'}


@app.put('/produtos', response_model=ProductSchemaOut, response_model_exclude_unset=True)
async def update_product(produto: ProductUpdate, db: AsyncSession = Depends(get_session)):
    return await product_services.update_product(db, produto)
