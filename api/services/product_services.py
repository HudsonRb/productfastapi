from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from api.models.estoque import Estoque
from api.models.produtos import Produtos
from api.schemas.product_schema import ProductSchemaIn, ProductUpdate
from api.services.estoque_services import get_estoque_by_id


async def create_product(db: AsyncSession, produto: ProductSchemaIn) -> Produtos:
    product_searched = await get_product_by_name(db=db, name=produto.nome)
    if product_searched:
        raise HTTPException(status_code=400, detail=f'Produto já existente ID: {product_searched.codigo}.')
    if produto.preco_de < produto.preco_por:
        raise HTTPException(status_code=400, detail=f'Preco_de não pode ser inferior ao preco_por')
    produto_db = Produtos(
        nome=produto.nome,
        preco_de=produto.preco_de,
        preco_por=produto.preco_por)
    db.add(produto_db)
    await db.commit()
    estoq = Estoque(estoque_total=produto.estoque.estoque_total,
                    estoque_de_corte=produto.estoque.estoque_de_corte,
                    estoque_disponivel=produto.estoque.estoque_total,
                    produto_id=produto_db.codigo)
    db.add(estoq)
    await db.commit()
    prod_searched_created = await get_product(db, produto_db.codigo)
    return prod_searched_created


async def get_product(db: AsyncSession, codigo: int):
    prod = await db.execute(
        select(Produtos).where(Produtos.codigo == codigo).options(selectinload(Produtos.estoque)))
    res = prod.scalars().first()
    if not res:
        raise HTTPException(404, 'Produto Não Encontrado')
    return res


async def get_all_products(db: AsyncSession):
    prods = await db.execute(select(Produtos).options(selectinload(Produtos.estoque)))
    res = prods.scalars().all()
    return res


async def get_product_by_name(db: AsyncSession, name: str) -> Produtos | None:
    prods = await db.execute(select(Produtos).where(Produtos.nome == name))
    return prods.scalars().first()


async def delete_product(db: AsyncSession, prod_id: int) -> None:
    prod = await get_product(db, prod_id)
    if not prod:
        raise HTTPException(404, 'Produto não encontrado.')
    estq = await get_estoque_by_id(db, prod.estoque.codigo)
    if not estq:
        raise HTTPException(500, 'Internal Server Error')
    await db.delete(prod)
    await db.delete(estq)
    await db.commit()


async def update_product(db: AsyncSession, produto: ProductUpdate):
    search = await db.execute(
        select(Produtos).where(Produtos.codigo == produto.codigo).options(selectinload(Produtos.estoque)))
    prod: Produtos = search.scalars().first()
    if not prod:
        raise HTTPException(404, 'Produto não encontrado.')
    if produto.preco_de and produto.preco_por and produto.preco_de < produto.preco_por:
        raise HTTPException(status_code=400, detail=f'Preco_de não pode ser inferior ao preco_por')
    if produto.estoque.estoque_total > prod.estoque.estoque_disponivel:
        raise HTTPException(status_code=400, detail='Estoque Total Maior que o disponível')
    if produto.preco_de:
        prod.preco_de = produto.preco_de
    if produto.preco_por:
        prod.preco_por = produto.preco_por
    if produto.nome:
        prod.nome = produto.nome

    prod.estoque.estoque_disponivel = prod.estoque.estoque_disponivel - produto.estoque.estoque_total
    prod.estoque.estoque_total = produto.estoque.estoque_total
    prod.estoque.estoque_de_corte = produto.estoque.estoque_de_corte
    await db.commit()
    return prod
