from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from api.models.estoque import Estoque


async def get_estoque_by_id(db: AsyncSession, codigo: int):
    estoque = await db.execute(select(Estoque).where(Estoque.codigo == codigo))
    res = estoque.scalars().first()
    return res
